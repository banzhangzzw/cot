# ifndef COT_PREPROCESSOR_SEQ_DETAIL_IS_EMPTY_H
# define COT_PREPROCESSOR_SEQ_DETAIL_IS_EMPTY_H

#include "preprocessor/size.h"
#include "preprocessor/bool.h"
#include "preprocessor/dec.h"

# define COT_PP_SEQ_DETAIL_IS_EMPTY(seq) \
	COT_PP_COMPL \
		( \
		COT_PP_SEQ_DETAIL_IS_NOT_EMPTY(seq) \
		) \
/**/

# define COT_PP_SEQ_DETAIL_IS_EMPTY_SIZE(size) \
	COT_PP_COMPL \
		( \
		COT_PP_SEQ_DETAIL_IS_NOT_EMPTY_SIZE(size) \
		) \
/**/

# define COT_PP_SEQ_DETAIL_IS_NOT_EMPTY(seq) \
	COT_PP_SEQ_DETAIL_IS_NOT_EMPTY_SIZE(COT_PP_SEQ_DETAIL_EMPTY_SIZE(seq)) \
/**/

# define COT_PP_SEQ_DETAIL_IS_NOT_EMPTY_SIZE(size) \
	COT_PP_BOOL(size) \
/**/

# define COT_PP_SEQ_DETAIL_EMPTY_SIZE(seq) \
	COT_PP_DEC(COT_PP_SEQ_SIZE(seq (0))) \
/**/

# endif


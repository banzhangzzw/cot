# ifndef COT_PREPROCESSOR_CONTROL_IIF_H
# define COT_PREPROCESSOR_CONTROL_IIF_H


#    define COT_PP_IF(cond, t, f) COT_PP_IF_I(cond, t, f)
#    define COT_PP_IF_I(cond, t, f) COT_PP_IIF(COT_PP_BOOL(cond), t, f)

# define COT_PP_IIF(bit, t, f) COT_PP_IIF_OO((bit, t, f))
# define COT_PP_IIF_OO(par) COT_PP_IIF_I par

# define COT_PP_IIF_I(bit, t, f) COT_PP_IIF_II(COT_PP_IIF_ ## bit(t, f))
# define COT_PP_IIF_II(id) id


# define COT_PP_IIF_0(t, f) f
# define COT_PP_IIF_1(t, f) t

# endif